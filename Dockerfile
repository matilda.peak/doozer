FROM python:3.8.6

ENV HOME /root
WORKDIR $HOME

# Pre-requisites for Docker, protoc and other tools
RUN apt-get update \
    && apt-get -y install \
         apt-utils \
         apt-transport-https \
         ca-certificates \
         git \
         gnupg2 \
         software-properties-common \
         unzip

# PYTHON ----------------------------------------------------------------------

RUN pip install --upgrade pip

# DOCKER ----------------------------------------------------------------------

# Add Docker’s official GPG key
RUN curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -
RUN apt-key fingerprint 0EBFCD88

# Install docker
RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable" \
    && apt-get update \
    && apt-get -y install docker-ce=18.06.3~ce~3-0~debian \
    && rm -rf /var/lib/apt/lists/*

# Install docker compose
RUN sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

# PROTOC ----------------------------------------------------------------------

# Install protoc
RUN curl -L -O https://github.com/google/protobuf/releases/download/v3.3.0/protoc-3.3.0-linux-x86_64.zip \
    && unzip protoc-3.3.0-linux-x86_64.zip -d /usr/local \
    && rm protoc-3.3.0-linux-x86_64.zip

# JDK -------------------------------------------------------------------------

WORKDIR /tmp
ENV JAVA_HOME /opt/java-1.8
ENV PATH $JAVA_HOME/bin:$PATH

COPY library/jdk-8u202-linux-x64.tar.gz /tmp
RUN tar -zxf /tmp/jdk-8u202-linux-x64.tar.gz -C /opt \
    && mv /opt/jdk1.8.0_202 $JAVA_HOME \
    && rm /tmp/jdk-8u202-linux-x64.tar.gz

# Scala expects this file
RUN touch $JAVA_HOME/release

# SCALA -----------------------------------------------------------------------

ENV SCALA_VERSION 2.12.3
ENV SCALA_HOME /opt/scala-$SCALA_VERSION
ENV PATH $SCALA_HOME/bin:$PATH

RUN curl -fsL http://downloads.typesafe.com/scala/$SCALA_VERSION/scala-$SCALA_VERSION.tgz \
    | tar xfz - -C /opt

# PACKER & TERRAFORM ----------------------------------------------------------

WORKDIR $HOME/bin
ADD library/packer_*.*.*_linux_amd64.tar.gz $HOME/bin/
ADD library/terraform_*.*.*_linux_amd64.tar.gz $HOME/bin/
ENV PATH $HOME/bin:$PATH

# ANSIBLE ---------------------------------------------------------------------

RUN pip install ansible==2.10.4

# YACKER ---------------------------------------------------------------------

RUN pip install matildapeak-yacker==2019.1

# CHROME & SELENIUM DRIVER ----------------------------------------------------

# Install Chrome
# (we use it in in our selenium-based testing in 'headless' mode)...
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
RUN apt-get update -yqqq \
    && apt-get install -y google-chrome-stable

# Add the Chrome WebDriver to the PATH...
# This must be compatible with the google-chrome-stable release
# (ver 87.0.4280.88-1 at the last build)
#
# See https://chromedriver.chromium.org
ENV CHROME_DRIVER_PATH $HOME/selenium/chrome-driver-87
WORKDIR $CHROME_DRIVER_PATH
COPY library/selenium/drivers/linux/chrome-driver-87/chromedriver $CHROME_DRIVER_PATH/
ENV PATH $PATH:$CHROME_DRIVER_PATH

# REPO SECURITY SCANNER -------------------------------------------------------

ENV SCANREPO_PATH $HOME/repo-security-scanner
WORKDIR $SCANREPO_PATH
COPY library/repo-security-scanner/linux/scanrepo $SCANREPO_PATH/
ENV PATH $PATH:$SCANREPO_PATH

# -----------------------------------------------------------------------------

WORKDIR $HOME
