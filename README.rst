###################################
A Container-based build environment
###################################

.. image:: https://gitlab.com/matilda.peak/doozer/badges/master/pipeline.svg
   :target: https://gitlab.com/matilda.peak/doozer
   :alt: Pipeline Status (doozer)

A build environment used in the GitLab CI process and shared by a number of
other Matilda Peak projects. It produces a Docker image that contains all
our build tools offering a unified cross-project build environment.

The image contains, amongst other things, the following: -

* Python 3.8.6
* Docker 18.09.3
* JDK 1.8.0
* Scala 2.12.3
* Protoc 3.3.0
* `Packer`_ 1.3.5
* `Terraform`_ 0.11.11
* `Ansible`_ 2.10.4
* `Yacker`_ 2019.1
* Headless chrome (v87) and a selenium chrome driver
* The UK Home Office Repository `Security Scanner`_ 0.4.0

Getting and creating the HashiCorp tools
========================================

The tar files are often distributed as ``.zip`` files. Simply decompress
and re-compress as ``.tar.gz``. The following example illustrates
what you'd do with terraform 0.11.8::

    $ wget https://releases.hashicorp.com/terraform/0.11.8/terraform_0.11.8_linux_amd64.zip
    $ unzip terraform_0.11.8_linux_amd64.zip
    $ tar -zcvf terraform_0.11.8_linux_amd64.tar.gz terraform

.. _Ansible: https://www.ansible.com
.. _Packer: https://www.packer.io
.. _Security Scanner: https://github.com/UKHomeOffice/repo-security-scanner
.. _Terraform: https://www.terraform.io
.. _Yacker: https://gitlab.com/matilda.peak/yacker
